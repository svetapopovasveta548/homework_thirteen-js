//1) setTimeout эта функция вызывается один раз,а setInterval функция вызывается постоянно через указаные промежуток времени

//2) да, сработаает потому что таймер будет равен 0

//3) для того чтобы оставить цикл



const images = [{
    "name": "img1",
    "src": "1.jpg"
  },
  {
    "name": "img2",
    "src": "2.jpg"
  },
  {
    "name": "img3",
    "src": "3.jpg"
  },
  {
    "name": "img4",
    "src": "4.png"
  },

];
let imgIndex = 0;

function changeImg() {
  document.getElementById("slideshow").src = images[imgIndex].src;
  if (images.length > imgIndex + 1) {
    imgIndex++;
  } else {
    imgIndex = 0;
  }
}

changeImg();
let showImg = setInterval(changeImg, 3000)

const btnStop = document.querySelector(".button-first");
const btnStart = document.querySelector(".button-second");

btnStop.onclick = function () {
  clearInterval(showImg);
}

btnStart.onclick = function () {
  let showImg = setInterval(changeImg, 3000);
}